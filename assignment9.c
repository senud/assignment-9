#include <stdio.h>

int main()
{
    FILE *infile;

     //open the file in write mode

    infile = fopen("assignment9.txt", "w");
    fprintf(infile, "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(infile);

    //open the file in read mode

    infile = fopen("assignment9.txt", "r");
    char C[100];

     //get input from the user

    fgets(C, 75, infile);
    printf("%s\n", C);
    fclose(infile);

     //open the file in append mode

    infile = fopen("assignment9.txt", "a");
    fprintf(infile, "\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(infile);

    return 0;
}

